package com.teamone.travel.dal.queries;

import com.orm.query.Condition;
import com.orm.query.Select;
import com.teamone.travel.dal.HandleQuery;
import com.teamone.travel.models.Event;
import com.teamone.travel.models.Trip;

/**
 * Created by Michael Clausing on 4/27/2016.
 */
public class HandleEventQuery implements HandleQuery<Event, DefineEventQuery> {

    @Override
    public Event handle(DefineEventQuery define) {

        Select<Event> select = Select.from(Event.class);
        if(define.byId != null)
        {
            select = select.where(Condition.prop("Id").eq(define.byId));
        }
        return select.first();
    }
}
