package com.teamone.travel.models;

import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.Ignore;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Michael Clausing on 3/7/2016.
 */
public class Event extends SugarRecord {

    @Column(name = "Name")
    private String name;

    @Column(name = "TripId")
    private long tripId;

    @Column(name = "PlaceId")
    private long placeId;

    @Column(name = "StartDate")
    private Date startDate;

    @Column(name = "EndDate")
    private Date endDate;

    @Column(name = "Notes")
    private String notes;

    @Column(name = "Priority")
    private int priority;

    @Column(name = "Deleted")
    private boolean deleted;

    public Event(String name)
    {
        this.name = name;
        this.priority = LOW;
        this.deleted = false;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Event() {}

    public Place getPlace() {
        return Place.findById(Place.class, this.placeId);
    }

    public Trip getTrip() {
        return Trip.findById(Trip.class, this.tripId);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPlaceId() {
        return placeId;
    }

    public void setPlaceId(long placeId) {
        this.placeId = placeId;
    }

    public long getTripId() {
        return tripId;
    }

    public void setTripId(long tripId) {
        this.tripId = tripId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public String toString(){ return this.name; }

    @Ignore
    public final static int LOW = 1;

    @Ignore
    public final static int NORMAL = 0;

    @Ignore
    public final static int HIGH = 2;

    public static Map<Integer, String> getPriorityList() {
        Map<Integer, String> priorityList = new HashMap<>();
        priorityList.put(NORMAL, "Normal");
        priorityList.put(LOW, "Low");
        priorityList.put(HIGH, "High");
        return priorityList;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getPriority() {
        return this.priority;
    }

    public String getPriorityName() {
        return getPriorityList().get(priority);
    }
}
