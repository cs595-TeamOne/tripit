package com.teamone.travel.utility;

import java.util.Date;

/**
 * Created by Michael Clausing on 5/2/2016.
 */
public class EndDateWrapper extends DateWrapper {

    public EndDateWrapper() {
        this.date = new Date();
        setTimeForEndOfDay();
    }

    public EndDateWrapper(int year, int month, int date) {
        this.date = new Date();
        setRealYear(year);
        setRealMonth(month);
        setRealDate(date);
        setTimeForEndOfDay();
    }

    private void setTimeForEndOfDay() {
        this.date.setHours(23);
        this.date.setMinutes(59);
        this.date.setSeconds(59);
    }
}
