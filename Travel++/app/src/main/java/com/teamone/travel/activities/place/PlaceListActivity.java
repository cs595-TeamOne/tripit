package com.teamone.travel.activities.place;

import com.teamone.travel.R;
import com.teamone.travel.dal.HandleQuery;
import com.teamone.travel.dal.queries.DefinePlacesQuery;
import com.teamone.travel.dal.queries.HandlePlacesQuery;
import com.teamone.travel.models.Place;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by Johnney on 4/20/2016.
 */
public class PlaceListActivity extends ListActivity {
    final private String NONE = "None";

    private List<Place> list;
    private ArrayAdapter<Place> listAdapter;

    private HandleQuery<List<Place>, DefinePlacesQuery> placeQuery;
    private DefinePlacesQuery placeListQuery;
    private Toolbar toolbar;


    public PlaceListActivity() {
        placeQuery = new HandlePlacesQuery();
        placeListQuery = new DefinePlacesQuery();
        placeListQuery.deleted = false;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_list);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Place List");

        refreshList();
    }

    public void createPlace(View view) {
        Intent intent = new Intent(this, CreatePlaceActivity.class);
        startActivityForResult(intent, 1);
    }

    public void refreshList() {

        list = placeQuery.handle(placeListQuery);

        listAdapter = new ArrayAdapter(getListView().getContext(), android.R.layout.simple_list_item_1, list);
        listAdapter.add(new Place(NONE));
        getListView().setAdapter(listAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_trip, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        long returnId;
        if(list.get(position).getName() == NONE )
        {
            returnId = -1;
        } else {
            returnId = list.get(position).getId();
        }
        Intent returnIntent = getIntent();
        returnIntent.putExtra("placeId", returnId);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @Override
    protected void  onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                refreshList();
            }else {
                CharSequence text = "Place was not saved.";
                int duration = Toast.LENGTH_SHORT;
                Context context = getApplicationContext();
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        } else {
            CharSequence text = "There was an error with Request";
            int duration = Toast.LENGTH_SHORT;
            Context context = getApplicationContext();
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }

}
