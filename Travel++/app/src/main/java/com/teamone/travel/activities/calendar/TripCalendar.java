package com.teamone.travel.activities.calendar;

/**
 * Created by Johnney on 4/20/2016.
 */


import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.roomorama.caldroid.CaldroidFragment;
import com.teamone.travel.R;
import com.teamone.travel.dal.HandleQuery;
import com.teamone.travel.dal.queries.DefineTripsQuery;
import com.teamone.travel.dal.queries.HandleTripsQuery;
import com.teamone.travel.models.Trip;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class TripCalendar extends AppCompatActivity{
    private final int DAY_IN_MILLSEC = 86400000;
    private CaldroidFragment caldroidFragment;
    private List<Trip> list;

    DefineTripsQuery defineTripsQuery;
    HandleQuery<List<Trip>, DefineTripsQuery> tripQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trip_calendar_view);
        caldroidFragment = new CaldroidFragment();
        Bundle args = new Bundle();
        Calendar cal = Calendar.getInstance();
        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        caldroidFragment.setArguments(args);

        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calendarView, caldroidFragment);
        t.commit();

        getTrips();
        setDates();
    }

    private void getTrips() {
        tripQuery = new HandleTripsQuery();
        defineTripsQuery = new DefineTripsQuery();
        defineTripsQuery.deleted = false;
        list = tripQuery.handle(defineTripsQuery);
    }

    private void setDates() {
        for(int i = 0; i < this.list.size(); i++) {
            Trip trip = this.list.get(i);

            long startDate = trip.getStartDate().getTime();
            long endDate = trip.getEndDate().getTime();

            while(startDate < endDate) {
                caldroidFragment.setBackgroundDrawableForDate(new ColorDrawable(Color.LTGRAY), new Date(startDate));
                startDate += DAY_IN_MILLSEC;
            }
        }
    }
}

