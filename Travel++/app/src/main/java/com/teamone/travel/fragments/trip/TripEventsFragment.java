package com.teamone.travel.fragments.trip;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.teamone.travel.R;
import com.teamone.travel.activities.event.EventDetailActivity;
import com.teamone.travel.activities.trip.EventListAdapter;
import com.teamone.travel.dal.HandleQuery;
import com.teamone.travel.dal.queries.DefineEventsQuery;
import com.teamone.travel.dal.queries.HandleEventsQuery;
import com.teamone.travel.models.Event;

import java.util.List;

public class TripEventsFragment extends Fragment {

    ListView eventsList;
    List<Event> list;
    HandleQuery<List<Event>, DefineEventsQuery> handleQuery;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_trip_events, container, false);

        eventsList = (ListView) view.findViewById(R.id.eventsListView);

        Bundle extras = getActivity().getIntent().getExtras();

        if(extras != null) {
            handleQuery = new HandleEventsQuery();
            DefineEventsQuery defineEventsQuery = new DefineEventsQuery();
            defineEventsQuery.byTripId = extras.getLong("tripID");
            defineEventsQuery.IsDeleted = false;
            list = handleQuery.handle(defineEventsQuery);
        }

        EventListAdapter adapter = new EventListAdapter(list, getContext());
        eventsList.setAdapter(adapter);

        eventsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(getActivity(), EventDetailActivity.class);
                intent.putExtra("eventID", list.get(position).getId());
                startActivityForResult(intent, 2);
            }

        });

        return view;
    }


}
