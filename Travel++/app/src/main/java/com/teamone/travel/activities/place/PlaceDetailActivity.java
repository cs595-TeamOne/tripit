package com.teamone.travel.activities.place;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.teamone.travel.R;
import com.teamone.travel.dal.HandleQuery;
import com.teamone.travel.dal.queries.DefinePlaceQuery;
import com.teamone.travel.dal.queries.HandlePlaceQuery;
import com.teamone.travel.models.Place;
import com.teamone.travel.utility.ViewPagerAdapter;
import com.teamone.travel.fragments.place.PlaceDetailsFragment;


/**
 * Created by Mark on 4/7/2016.
 */
public class PlaceDetailActivity extends AppCompatActivity{

    private ViewPager viewPager;
    private Place place;
    private PlaceDetailsFragment placeDetailsFragment = new PlaceDetailsFragment();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place);
        this.setTitle("Place " + place.getName());

        Bundle extras = getIntent().getExtras();

        if(extras != null && extras.containsKey("placeId") && (place = getPlace(extras.getLong("placeId"))) != null) {
            //getWindow(0.setSoftInputMode(WindowManger.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        } else {
            //showErrorDueToBundle();
        }


        viewPager = (ViewPager) findViewById(R.id.viewpager_place);
        setupViewPager(viewPager);

        //this hides the keyboard from automatically showing
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(placeDetailsFragment, "Details");
        viewPager.setAdapter(adapter);
    }


    public void deletePlaceClicked(View view)
    {
        //Set up a confirmation dialog asking if the user is certain they wish to delete
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete Place");
        builder.setMessage("Are you sure you want to delete this Place?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which) //Proceed with deletion logic if they select yes
            {
                dialog.dismiss();
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish(); //return to events screen
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing if they click no
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();

    }

    private Place getPlace(long id) {
        HandleQuery<Place, DefinePlaceQuery> handlePlaceQuery = new HandlePlaceQuery();
        DefinePlaceQuery definePlaceQuery = new DefinePlaceQuery();
        definePlaceQuery.byId = id;
        return handlePlaceQuery.handle(definePlaceQuery);
    }

    private void showErrorDueToBundle() {
        CharSequence text = "There was an error create/loading the Event Details screen. Please Try again.";
        int duration = Toast.LENGTH_SHORT;
        Context context = getApplicationContext();
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();

        Intent intent = new Intent();
        onActivityResult(0, 1, intent);
        finish();
    }

    public void editPlaceClicked(View view) {
        Intent intent = new Intent(this, EditPlaceActivity.class);
        intent.putExtra("placeId", place.getId());
        startActivityForResult(intent, 1);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        place = getPlace(place.getId());
        refreshPlaceDetails();
    }

    public void refreshPlaceDetails()
    {
        this.setTitle("Place " + place.getName());

        TextView placeNameTextView = (TextView) findViewById(R.id.placeName);
        TextView placeAddressTextView = (TextView) findViewById(R.id.placeAddress);
        TextView placeCityTextView = (TextView) findViewById(R.id.placeCity);
        TextView placeStateTextView = (TextView) findViewById(R.id.placeState);
        TextView placeZipTextView = (TextView) findViewById(R.id.placeZip);
        TextView placeWebsiteTextView = (TextView) findViewById(R.id.placeWebsite);
        TextView placePhoneTextView = (TextView) findViewById(R.id.placePhone);
        EditText placeNotesDetailsTextView = (EditText) findViewById(R.id.place_notes);
        CheckBox cb = (CheckBox) findViewById(R.id.placeFavoriteBox);

        if(place.getName() !=  null) {
            placeNameTextView.setText(place.getName());
        } else {
            placeNameTextView.setText("Place Name");
        }

        if(place.getAddress() !=  null) {
            placeAddressTextView.setText(place.getAddress().toString());
        } else {
            placeAddressTextView.setText("Place Address");
        }

        if(place.getCity() !=  null) {
            placeCityTextView.setText(place.getCity().toString());
        } else {
            placeCityTextView.setText("Place City");
        }

        if(place.getState() !=  null) {
            placeStateTextView.setText(place.getState().toString());
        } else {
            placeStateTextView.setText("Place State");
        }

        if(place.getZipCode() !=  null) {
            placeZipTextView.setText(place.getZipCode().toString());
        } else {
            placeZipTextView.setText("Place Name");
        }

        if(place.getWebsite() !=  null) {
            placeWebsiteTextView.setText(place.getWebsite().toString());
        } else {
            placeWebsiteTextView.setText("Place Website");
        }

        if(place.getPhoneNumber() !=  null) {
            placePhoneTextView.setText(place.getPhoneNumber().toString());
        } else {
            placePhoneTextView.setText("Place's Phone Number");
        }

        if(place.isFavorite()) {
            cb.setChecked(true);
        } else {
            cb.setChecked(false);
        }

        if(place.getNotes() !=  null) {
            placeNotesDetailsTextView.setText(place.getNotes());
        } else {
            placeNotesDetailsTextView.setText("");
        }
    }

}
