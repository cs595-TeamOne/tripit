package com.teamone.travel.dal.queries;

import com.orm.query.Condition;
import com.orm.query.Select;
import com.teamone.travel.dal.HandleQuery;
import com.teamone.travel.models.Trip;

/**
 * Created by Michael Clausing on 4/3/2016.
 */
public class HandleTripQuery implements HandleQuery<Trip, DefineTripQuery> {

    @Override
    public Trip handle(DefineTripQuery define) {

        Select<Trip> select = Select.from(Trip.class);
        if(define.byId != null)
        {
            select = select.where(Condition.prop("Id").eq(define.byId));
        }
        return select.first();
    }
}
