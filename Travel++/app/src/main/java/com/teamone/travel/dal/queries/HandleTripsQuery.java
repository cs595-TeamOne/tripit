package com.teamone.travel.dal.queries;

import com.orm.query.Condition;
import com.orm.query.Select;
import com.teamone.travel.dal.HandleQuery;
import com.teamone.travel.models.Trip;

import java.util.List;

/**
 * Created by Michael Clausing on 3/18/2016.
 */
public class HandleTripsQuery implements HandleQuery<List<Trip>, DefineTripsQuery> {

    @Override
    public List<Trip> handle(DefineTripsQuery define) {

        Select<Trip> query = Select.from(Trip.class);
        if(define.byState != null)
        {
            if(define.byState == DefineTripsQuery.stateOptions.PLANNED) {
                query = query.where(Condition.prop("State").eq(Trip.PLANNED));
            } else if(define.byState == DefineTripsQuery.stateOptions.CURRENT) {
                query = query.where(Condition.prop("State").eq(Trip.CURRENT));
            } else if(define.byState == DefineTripsQuery.stateOptions.COMPLETED) {
                query = query.where(Condition.prop("State").eq(Trip.COMPLETED));
            }
        }

        if(define.deleted != null) {
            if(define.deleted) {
                query = query.where(Condition.prop("Deleted").eq(1));
            } else {
                query = query.where(Condition.prop("Deleted").eq(0));
            }
        }
        return query.list();
    }
}
