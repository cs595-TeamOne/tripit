package com.teamone.travel.fragments.trip;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.teamone.travel.R;
import com.teamone.travel.dal.HandleQuery;
import com.teamone.travel.dal.queries.DefineTripQuery;
import com.teamone.travel.dal.queries.HandleTripQuery;
import com.teamone.travel.models.Trip;
import com.teamone.travel.utility.DateWrapper;


public class TripDetailsFragment extends Fragment {

    String name;
    View view;
    private Trip trip;
    private HandleQuery<Trip, DefineTripQuery> handleQuery;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_trip_details, container, false);

        Bundle extras = getActivity().getIntent().getExtras();
        if (extras != null) {
            name = extras.getString("tripName");
        }

        refresh(view);

        return view;
    }

    /**
     * Updates view with Trip information.
     * Loads from the database which may not be the best way, but it does guarantee that it is up to date.
     * @param view - view to be updated
     */
    private void refresh(View view)
    {
        Bundle extras = getActivity().getIntent().getExtras();
        if(extras != null) {
            handleQuery = new HandleTripQuery();
            DefineTripQuery defineTripQuery = new DefineTripQuery();
            defineTripQuery.byId = extras.getLong("tripID");
            trip = handleQuery.handle(defineTripQuery);

            if(trip != null) {

                TextView startDateText = (TextView) view.findViewById(R.id.startDateText);
                startDateText.setText(new DateWrapper(trip.startDate).toString());

                TextView endDateText = (TextView) view.findViewById(R.id.endDateText);
                endDateText.setText(new DateWrapper(trip.endDate).toString());

                CheckBox fav = (CheckBox)view.findViewById(R.id.favoriteCheckBox);
                fav.setChecked(trip.favorite);
            }
        }
    }
}
