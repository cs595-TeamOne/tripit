package com.teamone.travel.dal.queries;

import com.orm.query.Condition;
import com.orm.query.Select;
import com.teamone.travel.dal.HandleQuery;
import com.teamone.travel.models.Place;

import java.util.List;

/**
 * Created by Michael Clausing on 4/20/2016.
 */
public class HandlePlacesQuery implements HandleQuery<List<Place>, DefinePlacesQuery> {

    @Override
    public List<Place> handle(DefinePlacesQuery  define) {

        Select<Place> query = Select.from(Place.class);

        if(define.deleted != null) {
            if(define.deleted) {
                query = query.where(Condition.prop("Deleted").eq(1));
            } else {
                query = query.where(Condition.prop("Deleted").eq(0));
            }
        }
        return query.list();
    }
}
