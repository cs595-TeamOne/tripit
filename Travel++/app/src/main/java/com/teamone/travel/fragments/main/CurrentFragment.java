package com.teamone.travel.fragments.main;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.teamone.travel.R;

import com.teamone.travel.activities.main.TripListAdapter;
import com.teamone.travel.activities.trip.TripDetailActivity;
import com.teamone.travel.dal.HandleQuery;
import com.teamone.travel.dal.queries.DefineTripsQuery;
import com.teamone.travel.dal.queries.HandleTripsQuery;
import com.teamone.travel.models.Trip;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CurrentFragment extends Fragment {

    ListView currentList;
    List<Trip> list;
    View currView;
    DefineTripsQuery currentTripQuery;
    HandleQuery<List<Trip>, DefineTripsQuery> tripQuery;
    sortedListType sortType = sortedListType.NAME;

    public CurrentFragment() {
        tripQuery = new HandleTripsQuery();
        currentTripQuery = new DefineTripsQuery();
        currentTripQuery.byState = DefineTripsQuery.stateOptions.CURRENT;
        currentTripQuery.deleted = false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        currView = inflater.inflate(R.layout.fragment_current, container, false);
        currentList = (ListView) currView.findViewById(R.id.listViewCurrent);

        refreshList();

        currentList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // When clicked, begin trip details activity for that event
                Intent intent = new Intent(getActivity(), TripDetailActivity.class);
                intent.putExtra("tripID", list.get(position).getId());
                getActivity().startActivityForResult(intent, 2);
            }
        });

        return currView;
    }

    public void refreshList()
    {
        sortCurrTrips(sortType.value);
    }

    public void sortCurrTrips(int id)
    {
        list = tripQuery.handle(currentTripQuery);
        boolean sorted = false;

        if (id == R.id.sort_name) {
            Collections.sort(list, new Comparator<Trip>() {
                @Override
                public int compare(Trip t1, Trip t2) {
                    return t1.name.compareToIgnoreCase(t2.name);
                }
            });
            sortType = sortedListType.NAME;
            sorted = true;
        }

        else if (id == R.id.sort_start_date) {

            Collections.sort(list, new Comparator<Trip>() {
                @Override
                public int compare(Trip t1, Trip t2) {
                    return t1.startDate.compareTo(t2.startDate);
                }
            });
            sortType = sortedListType.STARTDATE;
            sorted = true;
        }

        else if (id == R.id.sort_end_date)
        {
            Collections.sort(list, new Comparator<Trip>() {
                @Override
                public int compare(Trip t1, Trip t2) {
                    return t1.endDate.compareTo(t2.endDate);
                }
            });
            sortType = sortedListType.ENDDATE;
            sorted = true;
        }

        else if (id == R.id.sort_favorite) {

            Collections.sort(list, new Comparator<Trip>() {
                @Override
                public int compare(Trip t1, Trip t2) {
                    return Boolean.compare(t2.favorite, t1.favorite);
                }
            });
            sortType = sortedListType.FAVORITE;
            sorted = true;
        }


        if (sorted)
        {
            //Only update the adapter if the list was sorted
            if(currentList != null) {
                TripListAdapter adapter = new TripListAdapter(list, getContext());
                currentList.setAdapter(adapter);
            }
        }
    }

    //enum indicating what order the list is sorted in
    public enum sortedListType
    {
        NAME(R.id.sort_name), STARTDATE(R.id.sort_start_date), ENDDATE(R.id.sort_end_date), FAVORITE(R.id.sort_favorite);
        private int value;

        sortedListType(int val)
        {
            this.value = val;
        }
    }

}
