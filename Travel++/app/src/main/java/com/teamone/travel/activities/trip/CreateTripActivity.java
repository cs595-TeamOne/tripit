package com.teamone.travel.activities.trip;

/**
 * Created by Mark on 3/15/2016.
 */
import java.util.Calendar;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.teamone.travel.R;
import com.teamone.travel.models.Trip;
import com.teamone.travel.utility.DateWrapper;
import com.teamone.travel.utility.EndDateWrapper;

public class CreateTripActivity extends AppCompatActivity {

    private Calendar calendar;

    private DateWrapper startDate;
    private DateWrapper endDate;
    private TextView startDateTextView;
    private TextView endDateTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_trip_screen);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Create Trip");
        setSupportActionBar(toolbar);

        startDateTextView = (TextView) findViewById(R.id.startDateText); //start text
        endDateTextView = (TextView) findViewById(R.id.endDateText); //start text

        calendar = Calendar.getInstance();
    }

    public void setDate(View view) {
        DateWrapper toUse = startDate == null ? new DateWrapper() : startDate;

        int year = toUse.getRealYear();
        int month = toUse.getRealMonth() - 1; // Requires offset because Android is stupid sometimes
        int day = toUse.getRealDate();

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, startDateListener, year, month, day);
        DatePicker datePicker = datePickerDialog.getDatePicker();
        datePicker.setMinDate(calendar.getTimeInMillis());
        datePickerDialog.show();
    }

    public void setDateEnd(View view) {
        DateWrapper toUse = endDate == null ? new DateWrapper() : endDate;

        int year = toUse.getRealYear();
        int month = toUse.getRealMonth() - 1; // Requires offset because Android is stupid sometimes
        int day = toUse.getRealDate();

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, endDateListener, year, month, day);
        DatePicker datePicker = datePickerDialog.getDatePicker();
        datePicker.setMinDate(calendar.getTimeInMillis());
        datePickerDialog.show();
    }

    /**
     * Listener for startDate DatePickerDialog
     */
    private DatePickerDialog.OnDateSetListener startDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            startDate = new DateWrapper(year, month + 1, day); // Requires offset because Android is stupid sometimes
            startDateTextView.setText(startDate.toString());
        }
    };

    /**
     * Listener for endDate DatePickerDialog
     */
    private DatePickerDialog.OnDateSetListener endDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            endDate = new EndDateWrapper(year, month + 1, day); // Requires offset because Android is stupid sometimes
            endDateTextView.setText(endDate.toString());
        }
    };

    public void finishClicked(View view)
    {
        TextView tv = (TextView) findViewById(R.id.editText);
        String name = tv.getText().toString();
        Context context = getApplicationContext();

        if(startDate != null && endDate != null && !name.equals("")) //these 3 fields are required, check if valid
        {
            if(startDate.toDate().before(endDate.toDate())) // confirm end date is after start date
            {
                try {
                    Trip trip1 = new Trip();
                    trip1.name = name;
                    trip1.startDate = startDate.toDate();
                    trip1.endDate = endDate.toDate();
                    trip1.save();
                    Intent intent = new Intent();
                    setResult(RESULT_OK, intent);
                    finish();
                } catch (Exception e) {
                    CharSequence text = "There was an error creating your trip. Please try again.";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
            }
            else
            {
                //data cannot be written
                CharSequence text = "Start Date must come before the end date.";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        }
        else
        {
            //data cannot be written
            CharSequence text = "Please provide a name, start date, and end date for your trip.";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }
}