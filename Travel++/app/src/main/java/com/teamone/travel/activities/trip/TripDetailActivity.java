package com.teamone.travel.activities.trip;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.Toast;

import com.teamone.travel.R;
import com.teamone.travel.activities.event.CreateEditEventActivity;

import com.teamone.travel.dal.HandleQuery;
import com.teamone.travel.dal.queries.DefineTripQuery;
import com.teamone.travel.dal.queries.HandleTripQuery;
import com.teamone.travel.fragments.trip.TripDetailsFragment;
import com.teamone.travel.fragments.trip.TripEventsFragment;
import com.teamone.travel.models.Trip;
import com.teamone.travel.utility.ViewPagerAdapter;

public class TripDetailActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    public Trip selectedTrip;
    private HandleQuery<Trip, DefineTripQuery> handleQuery;
    private int currentTabPage = 0;

    private TripEventsFragment tripEventsFragment;
    private TripDetailsFragment tripDetailsFragment;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip);
        toolbar = (Toolbar) findViewById(R.id.toolbar_trip);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            handleQuery = new HandleTripQuery();
            DefineTripQuery defineTripQuery = new DefineTripQuery();
            defineTripQuery.byId = extras.getLong("tripID");
            selectedTrip = handleQuery.handle(defineTripQuery);

            toolbar.setTitle("Trip X");

            // TODO check for null
            toolbar.setTitle(selectedTrip.name);

            // TODO check for null
            toolbar.setTitle(selectedTrip.name);
        }

        updateFragments();
        setSupportActionBar(toolbar);

        viewPager = (ViewPager) findViewById(R.id.viewpager_trip);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs_trip);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void updateFragments() {
        tripEventsFragment = new TripEventsFragment();
        tripDetailsFragment = new TripDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("tripID", selectedTrip.getId().intValue());
        tripDetailsFragment.setArguments(bundle);
        tripEventsFragment.setArguments(bundle);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(tripDetailsFragment, "Details");
        adapter.addFragment(tripEventsFragment, "Events");
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_trip, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.trip_delete) {
            deleteTrip();
        }

        return super.onOptionsItemSelected(item);
    }

    public void editTripClicked(View view)
    {
        currentTabPage = viewPager.getCurrentItem();
        Intent intent = new Intent(this, EditTripActivity.class);
        intent.putExtra("tripID", selectedTrip.getId());
        startActivityForResult(intent, 1);
    }

    private void deleteTrip() {
        //Set up a confirmation dialog asking if the user is certain they wish to delete
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete Trip");
        builder.setMessage("Are you sure you want to delete this Trip?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) //Proceed with deletion logic if they select yes
            {
                dialog.dismiss();
                Intent intent = new Intent();
                selectedTrip.setDeleted(true);
                selectedTrip.save();
                setResult(RESULT_OK, intent);
                finish(); //return to main activity (refreshes the listviews en route)
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing if they click no
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }


    public void onRadioButtonClicked(View view)
    {
        Context context = getApplicationContext();
        boolean checked = ((RadioButton) view).isChecked();

        CharSequence text;
        int duration = Toast.LENGTH_SHORT;
        Toast toast;

        switch(view.getId())
        {
            case R.id.currentRB:
                if (checked)
                    selectedTrip.state = Trip.CURRENT;
                selectedTrip.save();
                text = "This is now a Current Trip";
                toast = Toast.makeText(context, text, duration);
                toast.show();
                break;
            case R.id.completedRB:
                if (checked)
                    selectedTrip.state = Trip.COMPLETED;
                selectedTrip.save();
                text = "This Trip is now Completed";
                toast = Toast.makeText(context, text, duration);
                toast.show();
                break;
        }
    }

    public void addEventClick(View view)
    {
        currentTabPage = viewPager.getCurrentItem();
        Intent intent = new Intent(this, CreateEditEventActivity.class);
        intent.putExtra("tripID", selectedTrip.getId());
        startActivityForResult(intent, 1);
    }

    public void favoriteBoxChecked(View v) {
        Context context = getApplicationContext();
        CharSequence text;
        int duration = Toast.LENGTH_SHORT;
        Toast toast;
        if (((CheckBox) v).isChecked())
        {
            text = "This Trip is now a Favorite";
            toast = Toast.makeText(context, text, duration);
            toast.show();
            selectedTrip.favorite = true;
        }
        else
        {
            text = "This Trip is no longer a Favorite";
            toast = Toast.makeText(context, text, duration);
            toast.show();
            selectedTrip.favorite = false;
        }
        selectedTrip.save();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        refreshTripTabs();
        viewPager.setCurrentItem(currentTabPage);
    }

    public void refreshTripTabs() {
        updateFragments();
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(tripDetailsFragment, "Details");
        adapter.addFragment(tripEventsFragment, "Events");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

    }
}
