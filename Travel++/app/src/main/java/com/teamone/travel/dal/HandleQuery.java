package com.teamone.travel.dal;

/**
 * Created by Michael Clausing on 3/18/2016.
 */
public interface HandleQuery<TResult, DefineQuery> {
    TResult handle(DefineQuery define);
}
