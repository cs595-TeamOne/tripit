package com.teamone.travel.dal.queries;

import com.orm.query.Condition;
import com.orm.query.Select;
import com.teamone.travel.dal.HandleQuery;
import com.teamone.travel.models.Place;

/**
 * Created by Michael Clausing on 4/20/2016.
 */
public class HandlePlaceQuery implements HandleQuery<Place, DefinePlaceQuery> {

    @Override
    public Place handle(DefinePlaceQuery define) {

        Select<Place> select = Select.from(Place.class);
        if(define.byId != null)
        {
            select = select.where(Condition.prop("Id").eq(define.byId));
        }
        return select.first();
    }
}
