package com.teamone.travel.dal.queries;

import com.teamone.travel.dal.DefineQuery;

/**
 * Created by Michael Clausing on 3/18/2016.
 */
public class DefineTripsQuery implements DefineQuery {
    public enum stateOptions { PLANNED, CURRENT, COMPLETED };
    public Enum<stateOptions> byState;
    public Boolean deleted;
}
