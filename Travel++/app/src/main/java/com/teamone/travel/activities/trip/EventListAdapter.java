package com.teamone.travel.activities.trip;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.teamone.travel.R;
import com.teamone.travel.models.Event;

import java.util.List;

public class EventListAdapter extends BaseAdapter implements ListAdapter {
    private List<Event> list;
    private Context context;


    public EventListAdapter(List<Event> list, Context context)
    {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item, null);
        }

        TextView listItemText = (TextView)view.findViewById(R.id.listSel);
        listItemText.setText(list.get(position).getName());

        return view;
    }
}