package com.teamone.travel.activities.event;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.teamone.travel.R;
import com.teamone.travel.dal.HandleQuery;
import com.teamone.travel.dal.queries.DefineEventQuery;
import com.teamone.travel.dal.queries.HandleEventQuery;
import com.teamone.travel.models.Event;
import com.teamone.travel.models.Place;
import com.teamone.travel.utility.DateWrapper;

public class EventDetailActivity extends AppCompatActivity {

    private Event event;

    private Toolbar toolbar;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        Bundle extras = getIntent().getExtras();

        // Confirm Bundle exist, contains eventID, Event is not null
        if(extras != null && extras.containsKey("eventID") && (event = getEvent(extras.getLong("eventID"))) != null) {
            toolbar = (Toolbar) findViewById(R.id.toolbar);

            refreshEventDetails();

            //this hides the keyboard from automatically showing
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        } else {
            showErrorDueToBundle();
        }
    }

    private void showErrorDueToBundle() {
        CharSequence text = getString(R.string.event_detail_error);
        int duration = Toast.LENGTH_SHORT;
        Context context = getApplicationContext();
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();

        Intent intent = new Intent();
        onActivityResult(0, 1, intent);
        finish();
    }

    private Event getEvent(long id) {
        HandleQuery<Event, DefineEventQuery> handleEventQuery = new HandleEventQuery();
        DefineEventQuery defineEventQuery = new DefineEventQuery();
        defineEventQuery.byId = id;
        return handleEventQuery.handle(defineEventQuery);
    }

    public void deleteEventClicked(View view)
    {
        //Set up a confirmation dialog asking if the user is certain they wish to delete
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete Event");
        builder.setMessage("Are you sure you want to delete this Event?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which) //Proceed with deletion logic if they select yes
            {
                dialog.dismiss();
                Intent intent = new Intent();
                try {
                    event.setDeleted(true);
                    event.save();
                    setResult(RESULT_OK, intent);
                    finish(); //return to trips screen
                } catch(Exception e) {
                    // reset Event
                    event = getEvent(event.getId());
                }
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing if they click no
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void editEventClicked(View view)
    {
        Intent intent = new Intent(this, CreateEditEventActivity.class);
        intent.putExtra("eventID", event.getId());
        startActivityForResult(intent, 1);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        event = getEvent(event.getId());
        refreshEventDetails();
    }

    /**
     * Updated Event Details
     */
    public void refreshEventDetails()
    {
        toolbar.setTitle("Event " + event.getName());

        TextView startDateTextView = (TextView) findViewById(R.id.startDateText);
        TextView startTimeTextView = (TextView) findViewById(R.id.startTimeText);
        if(event.getStartDate() != null) {
            DateWrapper startDate = new DateWrapper(event.getStartDate());
            startDateTextView.setText(startDate.toString());
            startTimeTextView.setText(startDate.toTimeString());
        } else {
            startDateTextView.setText("");
            startTimeTextView.setText("");
        }

        TextView endDateTextView = (TextView) findViewById(R.id.endDateText);
        TextView endTimeTextView = (TextView) findViewById(R.id.endTimeText);
        if(event.getEndDate() != null) {
            DateWrapper endDate = new DateWrapper(event.getEndDate());
            endDateTextView.setText(endDate.toString());
            endTimeTextView.setText(endDate.toTimeString());
        } else {
            endDateTextView.setText("");
            endTimeTextView.setText("");
        }

        TextView priorityTextView = (TextView) findViewById(R.id.priorityText);
        priorityTextView.setText(event.getPriorityName());
        if(priorityTextView.getText().equals("High"))
        {
            priorityTextView.setTextColor(Color.RED);
        }
        else if (priorityTextView.getText().equals("Low"))
        {
            priorityTextView.setTextColor(Color.GREEN);
        }
        else
        {
            priorityTextView.setTextColor(Color.BLUE);
        }

        TextView placeTextView = (TextView) findViewById(R.id.placeText);
        Place place = event.getPlace();
        placeTextView.setText(place != null ? place.toString() : "");

        EditText notesTextView = (EditText) findViewById(R.id.notesText);
        notesTextView.setText(event.getNotes() != null ? event.getNotes() : "");

        setSupportActionBar(toolbar);
    }
}
