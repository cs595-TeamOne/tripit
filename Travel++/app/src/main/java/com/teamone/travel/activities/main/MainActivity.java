package com.teamone.travel.activities.main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.teamone.travel.R;

import com.teamone.travel.activities.calendar.TripCalendar;
import com.teamone.travel.activities.trip.CreateTripActivity;
import com.teamone.travel.fragments.main.CompletedFragment;
import com.teamone.travel.fragments.main.CurrentFragment;
import com.teamone.travel.fragments.main.PlanFragment;
import com.teamone.travel.utility.ViewPagerAdapter;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private PlanFragment planf = new PlanFragment();
    private CurrentFragment currentf = new CurrentFragment();
    private CompletedFragment completedf = new CompletedFragment();
    private int currentTabPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(planf, "Plan");
        adapter.addFragment(currentf, "Current");
        adapter.addFragment(completedf, "Completed");
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    //Toolbar menu item selection
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.new_trip) {
            currentTabPage = viewPager.getCurrentItem();
            Intent intent = new Intent(this, CreateTripActivity.class);
            startActivityForResult(intent, 1);
        }else if ( id == R.id.calendarView) {
            currentTabPage = viewPager.getCurrentItem();
            Intent intent = new Intent(this, TripCalendar.class);
            startActivityForResult(intent, 1);
        }
        else{
            sortTrips(id);
        }

        return super.onOptionsItemSelected(item);
    }


    //Called when we return from an activity (either TripDetailActivity or CreateTripActivity).
    //All we need to do is call refreshTabs, which will update each list with the proper Trips pulled from storage,
    //and subsequently update the adapter.
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        refreshTabs();
        viewPager.setCurrentItem(currentTabPage);
    }


    //refreshes the listViews within each fragment so they are reading the most up to date values IN STORAGE
    //Called whenever we ic_add, remove, or designate current/completed trips.
    public void refreshFragmentListsFromStorage()
    {
        planf.refreshList();
        currentf.refreshList();
        completedf.refreshList();
    }

    //re-adds the fragments to the tablayout, thus refreshing their appearance if trips were added to them recently.
    public void refreshTabs()
    {
        refreshFragmentListsFromStorage();
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(planf, "Plan");
        adapter.addFragment(currentf, "Current");
        adapter.addFragment(completedf, "Completed");
        viewPager.setAdapter(adapter);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void sortTrips(int id)
    {
        planf.sortPlanTrips(id);
        currentf.sortCurrTrips(id);
        completedf.sortCompTrips(id);
    }

    @Override
    public void onBackPressed()
    {
        new AlertDialog.Builder(this)
                .setTitle("Exit Travel++")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                    }

                }).create().show();
    }
}
