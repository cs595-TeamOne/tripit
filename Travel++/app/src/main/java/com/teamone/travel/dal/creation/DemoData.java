package com.teamone.travel.dal.creation;

import com.teamone.travel.models.Event;
import com.teamone.travel.models.Place;
import com.teamone.travel.models.Trip;
import com.teamone.travel.utility.DateWrapper;
import com.teamone.travel.utility.EndDateWrapper;

/**
 * Created by Michael Clausing on 3/30/2016.
 */
public class DemoData {

    private static final boolean WIPE_DATA = false;

    /**
     * Adds demo data if non currently exists
     */
    public static void addDemoData() {
        if(WIPE_DATA) {
            Place.deleteAll(Place.class);
            Event.deleteAll(Event.class);
            Trip.deleteAll(Trip.class);
        }

        if(Trip.count(Trip.class) == 0)
        {
            Trip trip1 = new Trip();
            trip1.name = "Capstone Rocks";
            DateWrapper today = new DateWrapper();
            trip1.startDate = today.toDate();
            DateWrapper endDate = new EndDateWrapper();
            trip1.endDate = endDate.toDate();
            trip1.state = Trip.CURRENT;
            trip1.favorite = true;
            trip1.save();

            Place place1 = new Place("Harley-Davidson Museum");
            place1.setAddress("400 W. Canal St.");
            place1.setCity("Milwaukee");
            place1.setState("Wisconsin");
            place1.save();

            Place place2 = new Place("Six Flags Great America");
            place2.setAddress("1 Great America Parkway");
            place2.setCity("Gurnee");
            place2.setState("Illinois");
            place2.save();

            Event event1 = new Event("Harley Davidson Tour");
            event1.setTripId(trip1.getId());
            event1.setPlaceId(place1.getId());
            event1.setNotes("Remember to get their early!");
            event1.setStartDate(new DateWrapper().toDate());
            event1.setEndDate(new DateWrapper().toDate());
            event1.save();

            Event event2 = new Event("Eat");
            event2.setTripId(trip1.getId());
            event2.save();

            Trip trip2 = new Trip();
            trip2.name = "New Years in Hawaii";
            trip2.startDate = new DateWrapper(2016,12,31).toDate();
            trip2.endDate = new EndDateWrapper(2017,1,7).toDate();
            trip2.state = Trip.PLANNED;
            trip2.save();

            Trip trip4 = new Trip();
            trip4.name = "July 4th Weekend";
            trip4.startDate = new DateWrapper(2016,7,1).toDate();
            trip4.endDate = new EndDateWrapper(2016,7,4).toDate();
            trip4.state = Trip.PLANNED;
            trip4.favorite = true;
            trip4.setDeleted(false);
            trip4.save();

            Trip tri5 = new Trip();
            tri5.name = "Thanksgiving in Denver";
            tri5.startDate = new DateWrapper(2016,11,23).toDate();
            tri5.endDate = new EndDateWrapper(2016,11,27).toDate();
            tri5.state = Trip.PLANNED;
            tri5.favorite = false;
            tri5.setDeleted(false);
            tri5.save();

            Trip trip6 = new Trip();
            trip6.name = "Annual Labor Day Cheese Factory Visit";
            trip6.startDate = new DateWrapper(2016,9,2).toDate();
            trip6.endDate = new EndDateWrapper(2016,9,5).toDate();
            trip6.state = Trip.PLANNED;
            trip6.favorite = false;
            trip6.setDeleted(false);
            trip6.save();


            Trip trip3 = new Trip();
            trip3.name = "Week off in January";
            trip3.startDate = new DateWrapper(2015,1,12).toDate();
            trip3.endDate = new EndDateWrapper(2015,1,19).toDate();
            trip3.state = Trip.COMPLETED;
            trip3.save();

            addExtraPlaces();
        }
    }

    private static void addExtraPlaces() {
        Place place1 = new Place("Mt Olympus Water & Theme Park"); place1.setAddress("1701 Wisconsin Dells Pkwy "); place1.setCity("Wisconsin Dells"); place1.setState("Wisconsin"); place1.setZipCode("53965"); place1.setPhoneNumber("8008004997"); place1.save();
        Place place2 = new Place("Noah's Ark"); place2.setAddress("1410 Wisconsin Dells Pkwy"); place2.setCity("Wisconsin Dells"); place2.setState("Wisconsin"); place2.setZipCode("53965"); place2.setPhoneNumber("6082546351"); place2.save();
        Place place3 = new Place("Dells Mining Co"); place3.setAddress("1480 Wisconsin Dells Pkwy"); place3.setCity("Wisconsin Dells"); place3.setState("Wisconsin"); place3.setZipCode("53965"); place3.setPhoneNumber("6082537002"); place3.save();
        Place place4 = new Place("Pirate's Cove Adventure Golf"); place4.setAddress("193 State Hwy 13"); place4.setCity("Wisconsin Dells"); place4.setState("Wisconsin"); place4.setZipCode("53965"); place4.setPhoneNumber("6082547500"); place4.save();
        Place place5 = new Place("Rick Wilcox Magic Theater"); place5.setAddress("1670 Wisconsin Dells Pkwy"); place5.setCity("Wisconsin Dells"); place5.setState("Wisconsin"); place5.setZipCode("53965"); place5.setPhoneNumber("6082545511"); place5.save();
        Place place6 = new Place("Bigfoot Zipline Tours"); place6.setAddress("1550 Wisconsin Dells Pkwy"); place6.setCity("Wisconsin Dells"); place6.setState("Wisconsin"); place6.setZipCode("53965"); place6.setPhoneNumber(""); place6.save();
        Place place7 = new Place("Hilton Garden Inn Wisconsin Dells"); place7.setAddress("101 E Hiawatha Dr"); place7.setCity("Wisconsin Dells"); place7.setState("Wisconsin"); place7.setZipCode("53965"); place7.setPhoneNumber("6082531100"); place7.save();
        Place place8 = new Place("AmericInn Lodge & Suites Wisconsin Dells"); place8.setAddress("550 State Hwy 13"); place8.setCity("Wisconsin Dells"); place8.setState("Wisconsin"); place8.setZipCode("53965"); place8.setPhoneNumber("6082541700"); place8.save();
        Place place9 = new Place("Best Western Ambassador Inn & Suites"); place9.setAddress("610 S Frontage Rd"); place9.setCity("Wisconsin Dells"); place9.setState("Wisconsin"); place9.setZipCode("53965"); place9.setPhoneNumber("6082544477"); place9.save();

    }
}
