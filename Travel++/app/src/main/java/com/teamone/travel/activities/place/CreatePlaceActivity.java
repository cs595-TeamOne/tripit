package com.teamone.travel.activities.place;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.teamone.travel.R;
import com.teamone.travel.models.Place;


/**
 * Created by Johnney on 4/6/2016.
 */
public class CreatePlaceActivity extends AppCompatActivity {

    private Place place;
    private String name;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_place_screen);

    }

    public void finishedPlaceEditClicked(View view)
    {
        this.place = new Place();
        TextView placeNameEditText = (TextView) findViewById(R.id.placeName);
        name = placeNameEditText.getText().toString();
        Context context = getApplicationContext();


        if(!name.equals("") && !name.equals(null)) //these fields are required, check if valid
        {
            EditText tv = (EditText) findViewById(R.id.placeName);

            EditText placeAddressEditView = (EditText) findViewById(R.id.placeAddress);
            EditText placeCityEditView = (EditText) findViewById(R.id.placeCity);
            EditText placeStateEditView = (EditText) findViewById(R.id.placeState);
            EditText placeZipEditView = (EditText) findViewById(R.id.placeZip);
            EditText placeWebsiteEditView = (EditText) findViewById(R.id.placeWebsite);
            EditText placePhoneEditView = (EditText) findViewById(R.id.placePhone);
            EditText placeNotesEditView = (EditText) findViewById(R.id.place_notes);

            name = tv.getText().toString();

                place.setName(name);
                if(placeAddressEditView.getText().toString() != null) {
                    place.setAddress(placeAddressEditView.getText().toString());
                }

                if(placeCityEditView.getText().toString() != null) {
                    place.setCity(placeCityEditView.getText().toString());
                }
                if(placeStateEditView.getText().toString() != null) {
                    place.setState(placeStateEditView.getText().toString());
                }
                if(placeZipEditView.getText().toString() != null) {
                    place.setZipCode(placeZipEditView.getText().toString());
                }
                if(placeWebsiteEditView.getText().toString() != null) {
                    place.setWebsite(placeWebsiteEditView.getText().toString());
                }
                if(placePhoneEditView.getText().toString() != null) {
                    place.setPhoneNumber(placePhoneEditView.getText().toString());
                }
                if(placeNotesEditView.getText().toString() != null) {
                    place.setNotes(placeNotesEditView.getText().toString());
                }

                place.save();

            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
        }
        else {
            //data cannot be written
            CharSequence text = "Please provide at least a name for your place.";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }
}
