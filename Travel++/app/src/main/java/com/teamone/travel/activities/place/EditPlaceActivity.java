package com.teamone.travel.activities.place;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.teamone.travel.R;
import com.teamone.travel.dal.HandleQuery;
import com.teamone.travel.dal.queries.DefinePlaceQuery;
import com.teamone.travel.dal.queries.HandlePlaceQuery;
import com.teamone.travel.models.Place;


/**
 * Created by Johnney on 4/6/2016.
 */
public class EditPlaceActivity extends AppCompatActivity {


    private Place place;
    private String name;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_place_details);

        Bundle extras = getIntent().getExtras();
        if(extras != null &&
                extras.containsKey("placeId") &&
                (place = getPlace(extras.getLong("placeId"))) != null) {

            place = getPlace(extras.getLong("placeId"));

            refreshEventDetails();

            //this hides the keyboard from automatically showing
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        } else {
            showErrorDueToBundle();
        }

        this.setTitle(place.getName());

    }

    public void finishedPlaceEditClicked(View view)
    {
        EditText tv = (EditText) findViewById(R.id.placeName);

        EditText placeAddressEditView = (EditText) findViewById(R.id.placeAddress);
        EditText placeCityEditView = (EditText) findViewById(R.id.placeCity);
        EditText placeStateEditView = (EditText) findViewById(R.id.placeState);
        EditText placeZipEditView = (EditText) findViewById(R.id.placeZip);
        EditText placeWebsiteEditView = (EditText) findViewById(R.id.placeWebsite);
        EditText placePhoneEditView = (EditText) findViewById(R.id.placePhone);
        EditText placeNotesEditView = (EditText) findViewById(R.id.edit_place_notes);

        name = tv.getText().toString();
        Context context = getApplicationContext();


        if(!name.equals("") && !name.equals(null)) //these fields are required, check if valid
        {
            place.setName(name);


            if(placeAddressEditView.getText().toString() != null) {
                place.setAddress(placeAddressEditView.getText().toString());
            }

            if(placeCityEditView.getText().toString() != null) {
                place.setCity(placeCityEditView.getText().toString());
            }
            if(placeStateEditView.getText().toString() != null) {
                place.setState(placeStateEditView.getText().toString());
            }
            if(placeZipEditView.getText().toString() != null) {
                place.setZipCode(placeZipEditView.getText().toString());
            }
            if(placeWebsiteEditView.getText().toString() != null) {
                place.setWebsite(placeWebsiteEditView.getText().toString());
            }
            if(placePhoneEditView.getText().toString() != null) {
                place.setPhoneNumber(placePhoneEditView.getText().toString());
            }
            if(placeNotesEditView.getText().toString() != null) {
                place.setNotes(placeNotesEditView.getText().toString());
            }

            place.save();
            Intent intent = new Intent();
            intent.putExtra("placeId", place.getId());
            setResult(RESULT_OK, intent);
            finish();
        }
        else
        {
            //data cannot be written
            CharSequence text = "Please provide at least a name for your place.";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }

    }

    private Place getPlace(long id){
        HandleQuery<Place, DefinePlaceQuery> handlePlaceQuery = new HandlePlaceQuery();
        DefinePlaceQuery definePlaceQuery = new DefinePlaceQuery();
        definePlaceQuery.byId = id;
        return handlePlaceQuery.handle(definePlaceQuery);
    }

    private void showErrorDueToBundle() {
        CharSequence text = "There was an error creating loading the Places details screen. Please try again.";
        int duration = Toast.LENGTH_SHORT;
        Context context = getApplicationContext();
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();

        Intent intent = new Intent();
        onActivityResult(0, 1, intent);
        finish();
    }

    private void refreshEventDetails() {
        this.setTitle("Place " + place.getName());
        EditText placeNameEditView = (EditText) findViewById(R.id.placeName);

        EditText placeAddressEditView = (EditText) findViewById(R.id.placeAddress);
        EditText placeCityEditView = (EditText) findViewById(R.id.placeCity);
        EditText placeStateEditView = (EditText) findViewById(R.id.placeState);
        EditText placeZipEditView = (EditText) findViewById(R.id.placeZip);
        EditText placeWebsiteEditView = (EditText) findViewById(R.id.placeWebsite);
        EditText placePhoneEditView = (EditText) findViewById(R.id.placePhone);
        EditText placeNotesEditView = (EditText) findViewById(R.id.edit_place_notes);
        CheckBox cb = (CheckBox) findViewById(R.id.placeFavoriteBox);

        if(place.getName().toString() != null){
            placeNameEditView.setText(place.getName().toString());
        }

        if(place.getAddress().toString() != null){
            placeAddressEditView.setText(place.getAddress().toString());
        }
        if(place.getCity().toString() != null){
            placeCityEditView.setText(place.getCity().toString());
        }
        if(place.getState().toString() != null){
            placeStateEditView.setText(place.getState().toString());
        }
        if(place.getZipCode() != null){
            placeZipEditView.setText(place.getZipCode().toString());
        }
        if(place.getWebsite() != null){
            placeWebsiteEditView.setText(place.getWebsite().toString());
        }
        if(place.getPhoneNumber() != null){
            placePhoneEditView.setText(place.getPhoneNumber().toString());
        }
        if(place.getNotes() != null){
            placeNotesEditView.setText(place.getNotes().toString());
        }

        if(place.isFavorite()) {
            cb.setChecked(true);
        } else {
            cb.setChecked(false);
        }
    }

    public void deletePlaceClicked(View view) {
        //Set up a confirmation dialog asking if the user is certain they wish to delete
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete Place");
        builder.setMessage("Are you sure you want to delete this Place?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which) //Proceed with deletion logic if they select yes
            {
                dialog.dismiss();
                Intent intent = new Intent();
                intent.putExtra("placeId", -1); // return minus 1 to represent no place
                try {
                    place.setDeleted(true);
                    place.save();
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                } catch(Exception e) {
                    CharSequence text = "An error occurred while deleting the Place.";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(getApplicationContext(), text, duration);
                    toast.show();
                    // reset Event
                    place = getPlace(place.getId());
                }
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing if they click no
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void placeFavoriteBoxChecked(View v) {

        if (((CheckBox) v).isChecked())
        {
            place.setFavorite(true);
        }
        else
        {
            place.setFavorite(false);
        }
        place.save();
    }
}
