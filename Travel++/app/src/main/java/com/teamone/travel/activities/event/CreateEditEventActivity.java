package com.teamone.travel.activities.event;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.teamone.travel.R;
import com.teamone.travel.activities.place.EditPlaceActivity;
import com.teamone.travel.activities.place.PlaceListActivity;
import com.teamone.travel.dal.HandleQuery;
import com.teamone.travel.dal.queries.DefineEventQuery;
import com.teamone.travel.dal.queries.DefinePlaceQuery;
import com.teamone.travel.dal.queries.DefineTripQuery;
import com.teamone.travel.dal.queries.HandleEventQuery;
import com.teamone.travel.dal.queries.HandlePlaceQuery;
import com.teamone.travel.dal.queries.HandleTripQuery;
import com.teamone.travel.models.Event;
import com.teamone.travel.models.Place;
import com.teamone.travel.models.Trip;
import com.teamone.travel.utility.DateWrapper;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.Map;

/**
 * Created by Mark on 4/6/2016.
 */
public class CreateEditEventActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private TextView startDateTextView;
    private TextView endDateTextView;
    private TextView startTimeTextView;
    private TextView endTimeTextView;
    private TextView noteTextView;
    private TextView nameTextView;
    private Spinner spinner;
    private TextView placeNameTextView;

    private DateWrapper startDate;
    private DateWrapper endDate;
    private Time startTime;
    private Time endTime;
    private String notes;
    private int priority;
    private String name;

    private long tripId;

    private Event event;
    private Place place;
    private Trip trip;

    private String event_id_key = "eventID";
    private String place_id_key = "placeID";
    private String event_start_date_key = "eventStartDate";
    private String event_end_date_key = "eventEndDate";
    private String event_start_time_key = "eventStartTime";
    private String event_end_time_key = "eventEndTime";
    private String event_priority_key = "eventPriority";

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_event_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        Bundle extras = getIntent().getExtras();

        if(extras == null ||
                (!extras.containsKey("tripID") &&
                        !(extras.containsKey("eventID") && (event = getEvent(extras.getLong("eventID"))) != null))) {
            errorDueToBundle();
        } else {
            if(extras.containsKey("tripID")) {
                toolbar.setTitle("Create Event");
                tripId = extras.getLong("tripID");
                trip = getTrip(tripId);
            } else {
                updatePrivateFields();
                toolbar.setTitle("Edit Event " + event.getName());
                trip = event.getTrip();
            }

            setSupportActionBar(toolbar);

            spinner = (Spinner) findViewById(R.id.spinner);
            spinner.setOnItemSelectedListener(this);

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, new ArrayList<>(Event.getPriorityList().values()));
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(dataAdapter);

            startDateTextView = (TextView) findViewById(R.id.startDateText); //start text
            endDateTextView = (TextView) findViewById(R.id.endDateText); //end text
            startTimeTextView = (TextView) findViewById(R.id.startTimeText); //end text
            endTimeTextView = (TextView) findViewById(R.id.endTimeText); //end text
            noteTextView = (TextView) findViewById(R.id.editEventNotes);
            nameTextView = (TextView) findViewById(R.id.editText);
            placeNameTextView = (TextView) findViewById(R.id.currentPlaceText);
            updateXmlItems();
        }
    }

    private void updateXmlItems() {
        if (startDate != null)
            startDateTextView.setText(startDate.toString());

        if (endDate != null)
            endDateTextView.setText(endDate.toString());

        if (startTime != null)
            startTimeTextView.setText(getDisplayForTime(startTime));

        if(endTime != null)
            endTimeTextView.setText(getDisplayForTime(endTime));

        if (Event.getPriorityList().containsKey(priority)) {
            String value = Event.getPriorityList().get(priority);
            for (int i = 0; i < spinner.getCount(); i++) {
                if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(value)) {
                    spinner.setSelection(i);
                    break;
                }
            }
        }

        if (notes != null)
            noteTextView.setText(notes);

        if (name != null)
            nameTextView.setText(name);

        if (place != null) {
            placeNameTextView.setText(place.getName());
        } else {
            placeNameTextView.setText("None");
        }
    }
    private void updatePrivateFields() {

        startDate = event.getStartDate() != null ? new DateWrapper(event.getStartDate()) : null;
        endDate = event.getEndDate() != null ? new DateWrapper(event.getEndDate()) : null;
        notes = event.getNotes();
        name = event.getName();
        priority = event.getPriority();

        if(startDate != null) {
            int hour = startDate.getRealHours();
            int minutes = startDate.getRealMinutes();
            startTime = new Time(hour, minutes, 0);
        }

        if(endDate != null) {
            int hour = endDate.getRealHours();
            int minutes = endDate.getRealMinutes();
            endTime = new Time(hour, minutes, 0);
        }

        place = event.getPlace();
    }

    private void errorDueToBundle() {
        CharSequence text = getString(R.string.event_bundle_error);
        int duration = Toast.LENGTH_SHORT;
        Context context = getApplicationContext();
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();

        Intent intent = new Intent();
        onActivityResult(0, 1, intent);
        finish();
    }

    private Event getEvent(long id) {
        HandleQuery<Event, DefineEventQuery> handleEventQuery = new HandleEventQuery();
        DefineEventQuery defineEventQuery = new DefineEventQuery();
        defineEventQuery.byId = id;
        return handleEventQuery.handle(defineEventQuery);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Iterator it = Event.getPriorityList().entrySet().iterator();
        String value = parent.getItemAtPosition(position).toString();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            if(pair.getValue() == value) {
                priority = (int) pair.getKey();
                break;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /**
     * Initializes startDate if needed and displays DatePickerDialog
     * @param view: Source View
     */
    public void setDate(View view) {
        DateWrapper toUse = startDate == null ? new DateWrapper(trip.startDate) : startDate;

        int year = toUse.getRealYear();
        int month = toUse.getRealMonth() - 1; // Requires offset because Android is stupid sometimes
        int day = toUse.getRealDate();

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, startDateListener, year, month, day);
        DatePicker datePicker = datePickerDialog.getDatePicker();
        datePicker.setMinDate(trip.startDate.getTime());
        datePicker.setMaxDate(trip.endDate.getTime());
        datePickerDialog.show();
    }

    /**
     * Initializes endDate if needed and displays DatePickerDialog
     * @param view: Source View
     */
    public void setDateEnd(View view) {
        DateWrapper toUse = endDate == null ? new DateWrapper(trip.endDate) : endDate;

        int year = toUse.getRealYear();
        int month = toUse.getRealMonth() - 1; // Requires offset because Android is stupid sometimes
        int day = toUse.getRealDate();
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, endDateListener, year, month, day);
        DatePicker datePicker = datePickerDialog.getDatePicker();
        datePicker.setMinDate(trip.startDate.getTime());
        datePicker.setMaxDate(trip.endDate.getTime());
        datePickerDialog.show();
    }

    /**
     * Listener for startDate DatePickerDialog
     */
    private DatePickerDialog.OnDateSetListener startDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            startDate = new DateWrapper(year, month + 1, day); // Requires offset because Android is stupid sometimes
            startDateTextView.setText(startDate.toString());
        }
    };

    /**
     * Listener for endDate DatePickerDialog
     */
    private DatePickerDialog.OnDateSetListener endDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            endDate = new DateWrapper(year, month + 1, day); // Requires offset because Android is stupid sometimes
            endDateTextView.setText(endDate.toString());
        }
    };

    @SuppressWarnings("deprecation")
    public void setTime(View view) {
        Time toUse = startTime == null ? new Time(0,0,0) : startTime;

        new TimePickerDialog(this, startTimeOnTimeSetListener, toUse.getHours(), toUse.getMinutes(), false).show();
    }

    @SuppressWarnings("deprecation")
    public void setEndTime(View view) {
        Time toUse = endTime == null ? new Time(0,0,0) : endTime;

        new TimePickerDialog(this, endTimeOnTimeSetListener, toUse.getHours(), toUse.getMinutes(), false).show();
    }

    /**
     * Listener for startTime TimePickerDialog
     */
    private TimePickerDialog.OnTimeSetListener startTimeOnTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            startTime = new Time(hourOfDay, minute, 0);
            startTimeTextView.setText(getDisplayForTime(startTime));
        }
    };

    /**
     * Formats Time for display in Activity
     * @param time: Time to be displayed
     * @return String formatted as hh:mm AM/PM
     */
    private String getDisplayForTime(Time time) {
        int displayHours = time.getHours() % 12 == 0 ? 12 : time.getHours() % 12;
        String amPm = time.getHours() > 11 ? "PM" : "AM";
        return String.format(Locale.US ,"%d:%02d %s", displayHours , startTime.getMinutes(), amPm);
    }

    /**
     * Listener for endTime TimePickerDialog
     */
    private TimePickerDialog.OnTimeSetListener endTimeOnTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            endTime = new Time(hourOfDay, minute, 0);
            endTimeTextView.setText(getDisplayForTime(endTime));
        }
    };

    /**
     * Finish Event creation
     * @param view: Activity
     */
    public void finishClicked(View view)
    {
        name = nameTextView.getText().toString();
        Context context = getApplicationContext();

        if(name.equals("")) { // Our requirements only show a requirement on name!
            // Name is required
            CharSequence text = "Please provide at least a name for your event.";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        } else {
            if (!DatesAreInCorrectOrder()) {
                // Dates should be in correct order (or null)
                CharSequence text = "Start Date must come before the end date.";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            } else {
                if(!DatesAreValid()) {
                    // Dates should be within range of trip (or null)
                    CharSequence text = "Event start or end time is not within range of the Trip.";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                } else {
                    try {
                        if (event == null)
                            event = new Event(name);

                        // Needs to be updated
                        notes = noteTextView.getText().toString();

                        event.setName(name);

                        if (startDate != null) {
                            if (startTime != null) {
                                startDate.setRealHours(startTime.getHours());
                                startDate.setRealMinutes(startTime.getMinutes());
                            } else {
                                startDate.setRealHours(0);
                                startDate.setRealMinutes(0);
                            }
                            event.setStartDate(startDate.toDate());
                        } else {
                            event.setStartDate(null);
                        }

                        if (endDate != null) {
                            if (endTime != null) {
                                endDate.setRealHours(endTime.getHours());
                                endDate.setRealMinutes(endTime.getMinutes());
                            } else {
                                endDate.setRealHours(23);
                                endDate.setRealMinutes(59);
                            }
                            event.setEndDate(endDate.toDate());
                        } else {
                            event.setEndDate(null);
                        }

                        if (tripId > 0)
                            event.setTripId(tripId);


                        event.setPriority(priority);
                        event.setNotes(notes);
                        if (place != null)
                            event.setPlaceId(place.getId());
                        else
                            event.setPlaceId(0);

                        event.save();

                        Intent intent = new Intent();
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                    catch (Exception e) {
                        CharSequence text = "There was an error saving your event. Please try again.";
                        int duration = Toast.LENGTH_SHORT;
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    }
                }
            }
        }
    }

    /**
     * Determine is startDate/endDate are valid
     * @return
     */
    private boolean DatesAreValid() {
        Trip trip = tripId > 0 ? getTrip(tripId) : event.getTrip();
        return (startDate == null || TimeUnit.MILLISECONDS.toDays(startDate.toDate().getTime() - trip.startDate.getTime()) >=0)
                && (endDate == null || TimeUnit.MILLISECONDS.toDays(trip.endDate.getTime() - endDate.toDate().getTime()) >= 0);
    }

    /**
     * Determine is startDate/endDate are in correct order
     * @return
     */
    private boolean DatesAreInCorrectOrder() {
        return startDate == null || endDate == null || TimeUnit.MILLISECONDS.toDays(endDate.toDate().getTime() - startDate.toDate().getTime()) >= 0;
    }

    private Trip getTrip(long tripId) {
        DefineTripQuery defineTripQuery = new DefineTripQuery();
        defineTripQuery.byId = tripId;
        return new HandleTripQuery().handle(defineTripQuery);
    }

    /**
     * When Change Place Button is pressed open another activity
     *
     * @param view: Activity
     */
    public void changePlace(View view)
    {
        Intent intent = new Intent(this, PlaceListActivity.class);
        startActivityForResult(intent, 1);
    }

    public void editPlaceClicked(View view)
    {
        Intent intent = new Intent(this, EditPlaceActivity.class);
        if(place != null && place.getId() != null) {
            intent.putExtra("placeId", place.getId());
            startActivityForResult(intent, 1);
        }else {
            CharSequence text = "There is no Place selected";
            int duration = Toast.LENGTH_SHORT;
            Context context = getApplicationContext();
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }

    /**
     *
     * Returning from PlaceList Activity
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void  onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                long placeId;
                try {
                    if(data.getExtras().get("placeId") instanceof Integer){
                        Integer tmpInt = (Integer) data.getExtras().get("placeId");
                        placeId = new Long(tmpInt);
                    } else {
                        placeId = (long) data.getExtras().get("placeId");
                    }

                    if(placeId > 0) {
                        DefinePlaceQuery definePlaceQuery = new DefinePlaceQuery();
                        definePlaceQuery.byId = placeId;
                        place = new HandlePlaceQuery().handle(definePlaceQuery);
                    } else {
                        place = null;
                    }

                } catch (Exception e) {
                    CharSequence text = "There was an error selecting/deleting Places";
                    int duration = Toast.LENGTH_SHORT;
                    Context context = getApplicationContext();
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
            } else {
                CharSequence text = "Place was not saved.";
                int duration = Toast.LENGTH_SHORT;
                Context context = getApplicationContext();
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        } else {
            CharSequence text = "There was an error with Request";
            int duration = Toast.LENGTH_SHORT;
            Context context = getApplicationContext();
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
        updateXmlItems();
    }

    private Place getPlace(long id){
        HandleQuery<Place, DefinePlaceQuery> handlePlaceQuery = new HandlePlaceQuery();
        DefinePlaceQuery definePlaceQuery = new DefinePlaceQuery();
        definePlaceQuery.byId = id;
        return handlePlaceQuery.handle(definePlaceQuery);
    }

    /**
     * Activity is most likely going to be recreated and fields are save to saveInstanceState
     * @param savedInstanceState: Bundle to be saved
     */
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        if(event != null)
            savedInstanceState.putLong(event_id_key, event.getId());

        if(place != null)
            savedInstanceState.putLong(place_id_key, place.getId());

        if(startDate != null)
            savedInstanceState.putLong(event_start_date_key, startDate.toDate().getTime());

        if(endDate != null)
            savedInstanceState.putLong(event_end_date_key, endDate.toDate().getTime());

        if(startTime != null)
            savedInstanceState.putLong(event_start_time_key, startTime.getTime());

        if(endTime != null)
            savedInstanceState.putLong(event_end_date_key, endTime.getTime());

        savedInstanceState.putInt(event_priority_key, priority);

        super.onSaveInstanceState(savedInstanceState);
    }

    /**
     * Activity is recreated and fields are set from savedInstanceState
     * @param savedInstanceState: saveInstanceState from onSaveInstanceState
     */
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if(savedInstanceState.containsKey(event_id_key))
            event = getEvent(savedInstanceState.getLong(event_id_key));
        else
            event = null;

        if(savedInstanceState.containsKey(place_id_key))
            place = getPlace(savedInstanceState.getLong(place_id_key));
        else
            place = null;

        if(savedInstanceState.containsKey(event_start_date_key))
            startDate = new DateWrapper(new Date(savedInstanceState.getLong(event_start_date_key)));
        else
            startDate = null;

        if(savedInstanceState.containsKey(event_end_date_key))
            endDate = new DateWrapper(new Date(savedInstanceState.getLong(event_end_date_key)));
        else
            endDate = null;

        if(savedInstanceState.containsKey(event_start_time_key))
            startTime = new Time(savedInstanceState.getLong(event_end_date_key));
        else
            startTime = null;


        if(savedInstanceState.containsKey(event_end_time_key))
            endTime = new Time(savedInstanceState.getLong(event_end_time_key));
        else
            endTime = null;

        if(savedInstanceState.containsKey(event_priority_key))
            priority = savedInstanceState.getInt(event_priority_key);

        super.onRestoreInstanceState(savedInstanceState);
    }
}
