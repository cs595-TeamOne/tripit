package com.teamone.travel;

import android.app.Application;

import com.orm.SugarContext;
import com.teamone.travel.dal.creation.DemoData;

/**
 * Created by Michael Clausing on 3/30/2016.
 * Added to play nice with Sugar ORM and add demo data if needed.
 */
public class TravelApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Required for Sugar ORM
        SugarContext.init(this);

        // Allows us to add demo data
        DemoData.addDemoData();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        // Required for Sugar ORM
        SugarContext.terminate();
    }
}
