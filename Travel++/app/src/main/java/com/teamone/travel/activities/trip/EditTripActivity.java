package com.teamone.travel.activities.trip;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.teamone.travel.R;
import com.teamone.travel.dal.HandleQuery;
import com.teamone.travel.dal.queries.DefineTripQuery;
import com.teamone.travel.dal.queries.HandleTripQuery;
import com.teamone.travel.models.Trip;
import com.teamone.travel.utility.DateWrapper;

import java.util.concurrent.TimeUnit;

public class EditTripActivity extends AppCompatActivity {

    private TextView startDateView;
    private TextView endDateView;
    private EditText nameEditText;
    private Toolbar toolbar;

    private Trip trip;
    private HandleQuery<Trip, DefineTripQuery> handleQuery;
    private DateWrapper startDate;
    private DateWrapper endDate;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_trip_details);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Edit Trip");
        setSupportActionBar(toolbar);

        Bundle extras = getIntent().getExtras();
        handleQuery = new HandleTripQuery();
        DefineTripQuery defineTripQuery = new DefineTripQuery();
        defineTripQuery.byId = extras.getLong("tripID");

        trip = handleQuery.handle(defineTripQuery);
        name = trip.name;
        startDate = new DateWrapper(trip.startDate);
        endDate = new DateWrapper(trip.endDate);
        updateDisplayDetails();
    }

    private void updateDisplayDetails() {
        nameEditText = (EditText) findViewById(R.id.editText); //name text
        nameEditText.setText(name);
        startDateView = (TextView) findViewById(R.id.startDateText); //start text
        startDateView.setText(startDate.toString());
        endDateView = (TextView) findViewById(R.id.endDateText); //end text
        endDateView.setText(endDate.toString());
    }

    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
    }

    @SuppressWarnings("deprecation")
    public void setDateEnd(View view) {
        showDialog(1000);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == 999) {
            int year = startDate.getRealYear();
            int month = startDate.getRealMonth() - 1; // Requires offset because Android is stupid sometimes
            int day = startDate.getRealDate();
            return new DatePickerDialog(this, startDateListener, year, month, day);
        }
        if (id == 1000) {
            int year = endDate.getRealYear();
            int month = endDate.getRealMonth() - 1; // Requires offset because Android is stupid sometimes
            int day = endDate.getRealDate();
            return new DatePickerDialog(this, endDateListenter, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener startDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            startDate = new DateWrapper(year, month + 1, day); // Requires offset because Android is stupid sometimes
            updateDisplayDetails();
        }
    };

    private DatePickerDialog.OnDateSetListener endDateListenter = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            endDate = new DateWrapper(year, month + 1, day); // Requires offset because Android is stupid sometimes
            updateDisplayDetails();
        }
    };

    public void finishClicked(View view)
    {
        TextView tv = (TextView) findViewById(R.id.editText);
        String name = tv.getText().toString();
        Context context = getApplicationContext();

        if(startDate != null && endDate != null && !name.equals(null))
        {
            long daysBetween = TimeUnit.MILLISECONDS.toDays(endDate.toDate().getTime() - startDate.toDate().getTime());
            if (daysBetween >= 0)
            {
                try {
                    trip.name = name;
                    trip.startDate = startDate.toDate();
                    trip.endDate = endDate.toDate();
                    trip.save();
                    Intent intent = new Intent();
                    setResult(RESULT_OK, intent);
                    finish();
                } catch (Exception e) {
                    CharSequence text = "There was an error editing your trip. Please try again.";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
            }
            else
            {
                //data cannot be written
                CharSequence text = "Start Date must come before the end date.";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        }
        else
        {
            //data cannot be written
            CharSequence text = "Please provide a name, start date, and end date for your event.";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }
}