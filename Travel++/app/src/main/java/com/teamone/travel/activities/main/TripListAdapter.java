package com.teamone.travel.activities.main;

/**
 * Created by Mark on 3/14/2016.
 *
 * This class is for a custom list item implementation with a Trip name and a subtext detailing the start and end dates.
 */

        import android.content.Context;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.BaseAdapter;
        import android.widget.ImageView;
        import android.widget.ListAdapter;
        import android.widget.TextView;

        import com.teamone.travel.R;
        import com.teamone.travel.models.Trip;
        import com.teamone.travel.utility.DateWrapper;

        import java.util.ArrayList;
        import java.util.List;

public class TripListAdapter extends BaseAdapter implements ListAdapter {
    private List<Trip> list = new ArrayList<Trip>();
    private Context context;


    public TripListAdapter(List<Trip> list, Context context)
    {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.custom_list_item, null);
        }

        ImageView img = (ImageView)view.findViewById(R.id.favoriteImage);
        if(list.get(position).favorite == true)
        {
            img.setVisibility(View.VISIBLE);
        }
        else
        {
            img.setVisibility(View.INVISIBLE);
        }

        TextView listItemText = (TextView)view.findViewById(R.id.firstLine);
        listItemText.setText(list.get(position).name);
        TextView listItemText2 = (TextView)view.findViewById(R.id.secondLine);
        listItemText2.setText(
                new DateWrapper(list.get(position).startDate).getRealMonth() + "/" +
                new DateWrapper(list.get(position).startDate).getRealDate() + "/" +
                new DateWrapper(list.get(position).startDate).getRealYear() +
                "    to    " +
                new DateWrapper(list.get(position).endDate).getRealMonth() + "/" +
                new DateWrapper(list.get(position).endDate).getRealDate() + "/" +
                new DateWrapper(list.get(position).endDate).getRealYear()
        );

        return view;
    }
}