package com.teamone.travel.models;

import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.Ignore;

import java.util.Date;
import java.util.List;

/**
 * Created by  Michael Clausing on 3/7/2016.
 */
public class Trip extends SugarRecord {

    @Column(name = "Name")
    public String name;

    @Column(name = "StartDate")
    public Date startDate;

    @Column(name = "EndDate")
    public Date endDate;

    @Column(name = "Favorite")
    public boolean favorite;

    @Column(name = "State")
    public int state;

    public Trip() {
        this.state = this.PLANNED;
        this.deleted = false;
    }

    public List<Event> getEvents() {
        return Event.find(Event.class, "TripId = ?", getId().toString());
    }

    public Trip(String name, Date startDate, Date endDate) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.state = this.PLANNED;
        this.deleted = false;
    }

    public String toString()
    {
        return name;
    }

    @Column(name = "Deleted")
    private boolean deleted;

    public boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Date getStartDate(){ return this.startDate; }

    public void setStartDate(Date startDate) { this.startDate = startDate;}

    public Date getEndDate(){ return this.endDate; }

    public void setEndDate(Date endDate) { this.endDate = endDate;}


    @Ignore
    public final static int PLANNED = 0;

    @Ignore
    public final static int CURRENT = 1;

    @Ignore
    public final static int COMPLETED = 2;
}
