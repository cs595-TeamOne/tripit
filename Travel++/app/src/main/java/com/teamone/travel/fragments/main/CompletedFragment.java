package com.teamone.travel.fragments.main;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.teamone.travel.R;

import com.teamone.travel.activities.main.TripListAdapter;
import com.teamone.travel.activities.trip.TripDetailActivity;
import com.teamone.travel.dal.HandleQuery;
import com.teamone.travel.dal.queries.DefineTripsQuery;
import com.teamone.travel.dal.queries.HandleTripsQuery;
import com.teamone.travel.models.Trip;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CompletedFragment extends Fragment {

    ListView completedList;
    List<Trip> list;
    View compView;
    DefineTripsQuery completedTripsQuery;
    HandleQuery<List<Trip>, DefineTripsQuery> tripQuery;
    sortedListType sortType = sortedListType.NAME;

    public CompletedFragment() {
        tripQuery = new HandleTripsQuery();
        completedTripsQuery = new DefineTripsQuery();
        completedTripsQuery.byState = DefineTripsQuery.stateOptions.COMPLETED;
        completedTripsQuery.deleted = false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        compView = inflater.inflate(R.layout.fragment_completed, container, false);
        completedList = (ListView) compView.findViewById(R.id.listViewCompleted);

        refreshList();

        TripListAdapter adapter = new TripListAdapter(list, getContext());
        completedList.setAdapter(adapter);

        completedList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // When clicked, begin trip details activity for that event
                Intent intent = new Intent(getActivity(), TripDetailActivity.class);
                intent.putExtra("tripID", list.get(position).getId());
                getActivity().startActivityForResult(intent, 2);
            }


        });

        return compView;
    }

    public void refreshList()
    {
        sortCompTrips(sortType.value);
    }

    public void sortCompTrips(int id)
    {
        list = tripQuery.handle(completedTripsQuery);
        boolean sorted = false;

        if (id == R.id.sort_name) {
            Collections.sort(list, new Comparator<Trip>() {
                @Override
                public int compare(Trip t1, Trip t2) {
                    return t1.name.compareToIgnoreCase(t2.name);
                }
            });
            sortType = sortedListType.NAME;
            sorted = true;
        }

        else if (id == R.id.sort_start_date) {

            Collections.sort(list, new Comparator<Trip>() {
                @Override
                public int compare(Trip t1, Trip t2) {
                    return t1.startDate.compareTo(t2.startDate);
                }
            });
            sortType = sortedListType.STARTDATE;
            sorted = true;
        }

        else if (id == R.id.sort_end_date)
        {
            Collections.sort(list, new Comparator<Trip>() {
                @Override
                public int compare(Trip t1, Trip t2) {
                    return t1.endDate.compareTo(t2.endDate);
                }
            });
            sortType = sortedListType.ENDDATE;
            sorted = true;
        }

        else if (id == R.id.sort_favorite) {

            Collections.sort(list, new Comparator<Trip>() {
                @Override
                public int compare(Trip t1, Trip t2) {
                    return Boolean.compare(t2.favorite, t1.favorite);
                }
            });
            sortType = sortedListType.FAVORITE;
            sorted = true;
        }


        if (sorted)
        {
            //Only update the adapter if the list was sorted
            if(completedList != null) {
                TripListAdapter adapter = new TripListAdapter(list, getContext());
                completedList.setAdapter(adapter);
            }
        }
    }

    //enum indicating what order the list is sorted in
    public enum sortedListType
    {
        NAME(R.id.sort_name), STARTDATE(R.id.sort_start_date), ENDDATE(R.id.sort_end_date), FAVORITE(R.id.sort_favorite);
        private int value;

        sortedListType(int val)
        {
            this.value = val;
        }
    }

}
