package com.teamone.travel.models;

import com.orm.SugarRecord;
import com.orm.dsl.Column;

/**
 * Created by Michael Clausing on 3/7/2016.
 */
public class Place extends SugarRecord {

    public Place() { }

    public Place(String name)
    {
        this.setName(name);
        this.setDeleted(false);
    }

    @Column(name = "Name")
    private String name;

    @Column(name = "Address")
    private String address;

    @Column(name = "City")
    private String city;

    @Column(name = "State")
    private String state;

    @Column(name = "ZipCode")
    private String zipCode;

    @Column(name = "PhoneNumber")
    private String phoneNumber;

    @Column(name = "Notes")
    private String notes;

    public String getName() {
        return name;
    }

    @Column(name = "Deleted")
    private boolean deleted;

    @Column(name = "Favorite")
    private boolean favorite;

    @Column(name = "Website")
    private String website;

    public boolean getDeleted() {
        return this.isDeleted();
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public  String getWebsite() { return website;}

    public void setWebsite(String website) { this.website = website;}

    @Override
    public String toString() { return this.name; }
}
