package com.teamone.travel.utility;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Michael Clausing on 4/11/2016.
 */
public class DateWrapper {

    protected Date date;

    public DateWrapper() {
        this.date = new Date();
        this.date.setHours(0);
        this.date.setMinutes(0);
        this.date.setSeconds(0);
    }

    public DateWrapper(Date date) {
        this.date = date;
    }

    public DateWrapper(int year, int month, int date) {
        this.date = new Date();
        setRealYear(year);
        setRealMonth(month);
        setRealDate(date);
        this.date.setHours(0);
        this.date.setMinutes(0);
        this.date.setSeconds(0);
    }

    public Date toDate() {
        return this.date;
    }

    public int getRealYear() {
        return this.date.getYear() + 1900;
    }

    public void setRealYear(int year) {
        this.date.setYear(year - 1900);
    }


    public int getRealMonth() {
        return this.date.getMonth() + 1;
    }

    public void setRealMonth(int month) {
        this.date.setMonth(month - 1);
    }

    public int getRealDate() {
        return this.date.getDate();
    }

    public void setRealDate(int day) {
        this.date.setDate(day);
    }

    public String toString() {
        SimpleDateFormat desiredFormat = new SimpleDateFormat("M/d/yyyy");
        return desiredFormat.format(this.date);
    }

    public int getRealHours() {
        return date.getHours();
    }

    public void setRealHours(int hours) {
        date.setHours(hours);
    }

    public int getRealMinutes() {
        return date.getMinutes();
    }

    public void setRealMinutes(int minutes) {
        date.setMinutes(minutes);
    }

    public String toTimeString() {
        SimpleDateFormat desiredFormat = new SimpleDateFormat("hh:mm aa");
        return desiredFormat.format(this.date);
    }
}
