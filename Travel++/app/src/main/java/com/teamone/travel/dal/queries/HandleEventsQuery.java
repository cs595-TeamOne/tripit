package com.teamone.travel.dal.queries;

import com.orm.query.Condition;
import com.orm.query.Select;
import com.teamone.travel.dal.HandleQuery;
import com.teamone.travel.models.Event;

import java.util.List;

/**
 * Created by Michael Clausing on 4/4/2016.
 */
public class HandleEventsQuery implements HandleQuery<List<Event>, DefineEventsQuery> {

    @Override
    public List<Event> handle(DefineEventsQuery define) {
        Select<Event> select = Select.from(Event.class);
        if(define.byTripId != null) {
            select = select.where(Condition.prop("TripId").eq(define.byTripId));
        }
        if(define.IsDeleted != null) {
            if(define.IsDeleted) {
                select = select.where(Condition.prop("Deleted").eq(1));
            } else {
                select = select.where(Condition.prop("Deleted").eq(0));
            }
        }
        return select.list();
    }
}
